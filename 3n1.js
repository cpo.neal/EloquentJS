const rl = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
});
rl.question('Please input a number. ', answer => {
console.log(answer + ' takes ' + length3n1(answer) + ' iterations to reach 1');
});
function length3n1(num) {
  let seq = [num,1];
  let count = 1;
  let presentNum = num;
  do {
    if (presentNum % 2 == 0) {
      presentNum = presentNum / 2;
    } else {
      presentNum = (presentNum * 3) + 1;
    }
    seq[count] = ' ' + presentNum;
    count = count + 1;
  } while (presentNum != 1);
  rl.close();
  console.log("The number's sequence is " + seq);
  return(count);
}
