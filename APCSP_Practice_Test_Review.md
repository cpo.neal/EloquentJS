# APCSP Practice Test Review
### Caleb O'Neal
## Question 1: App Colaboration
**Question:** What is a good example of using collaboration to avoid bias in the development of an application?<br>
**Answer:** Sending a survey to a students from multiple schools for feedback<br>
**Explnation of answer:** Recieving feedback from a large group of people who likely have varied interests and opinions is a good way to attempt to eliminate bias because people with differing opinions are better at catching things that they donot agree with because of our human tendency towards noticing the negative things that we see rather than the positives.<br>
**Explanation of my mistake:** I interpreted collaboration to mean a group of people workig on the app and not a survey which is why my answer was that you would code one half of the app and a partner would code the other half which is not collaboration because just splitting the work doesn't allow for communication between the programmers and your biases could also affect whou your partner is.
## Question 2: Iteration Mystery
**Question:** Consider the following code segment

number ← 1>
step  ← 1

REPEAT UNTIL (number > 16)
\{
  number ← number + step
  step ← step + 1
\}

DISPLAY(number)

What will be displayed after this segment is executed?<br>
**Answer:** 22<br>
**Explanation of answer:** 22 is the result of running the code the best way to see this is to make a table on a peice of scratch paper and to follow the code be hand to ensure that you get the correct answer.<br>
**Explanation of my mistake:** My answer was 21, I think I got that answer because i cofused the order when I was thinking through the first iteration of the loop. The solution to my mistake is just to double check my answers and to use scratch paper and make a table that goes through the process of the code. 
## Question 3
**Question:** What is a plausible data privacy concern of the new upgraded system?<br>
**Answer:** The toy company could provide the customer’s order history to other companies who are interested in offering complementary products to the customer.<br>
**Explanation of answer:** This answer is correct because data is being provided to another company and if any company would do this I would find it hilarious because they're providing data to another company free of charge and secondly scary because of the possibility that other companies are getting scammed in this way.<br>
**Explanation of my mistake:** When I was taking the test I interpreted it as providing data to the seller of the product not another company or individual that is recieving the data.
## Summary of my mistakes
My mistakes mainly stemmed from misinterpreting a question or answer and a lack of thoroughness when answering a question. My mistakes could be fixed by using strategies like using scratch paper or rereading the question and its answers so that I can reevaluate my answers.
P.S. Most of my other mistakes were similar or even more trivial and honestly I'm kind of dissapointed at the number of stupid mistakes I made.
