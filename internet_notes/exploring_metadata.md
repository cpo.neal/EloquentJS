# Metadata Notes
### Caleb and Madison
- Metadata is data about data 
- In code.org's data sets the metadata includes a description of the data set, the source of the data, what if any processing was done to the data for example on the words dataset the removed words not appropriate for a school setting, and what collumns are in the data set
- metadata has been used since before computer to organize and describe physical data and that same idea is now used for electronic data albiet at a mucher larger scale.
- In the code.org example they did not provide some metadata that could be nice to have such as the number of rows in the data set and the date that the data set was last updated although both could be found through a little bit of research
- In google trends the main metadata we have access to is location by state, city, or country, but you can't sort by things like the browser being used or the number of searches made about the topic, they just give you a percentage of the peak
