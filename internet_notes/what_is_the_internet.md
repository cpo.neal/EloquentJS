# What is the Internet
Caleb, Maggie, Spencer, and Remi
-------------------------------

What we already know about the internet/What we think

1. It can communicate data wirelessly
2. It contains a lot of data
3. IP addresses exist
4. When you search something your device will contact the DNS server specified     by your device which tells your device the IP address of the other device       that you're trying to connect with
 
  What we learned

1. When you first search something your query is sent to your ISP who then         process your query through a DNS(Domain Name Server) which translates the       query into an IP address 
2. After your computer atains the target IP address from the DNS it sends a        Hypertext Transfer Protocol(HTTP) request once approved the server will send    the data required to load the webpage in packets and the packages are then      assembeled into a webpage
3. Approximatly two zettabytes of data were transfered across the internet in      2019.
