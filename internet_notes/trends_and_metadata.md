# Google Trends
Group Members: Caleb and Madison
## The Olympics
- The data follows a pattern, the number of searches spikes dramatically when   the Olympics occur and spikes more during the summer Olympics.
- Using this pattern we can assume that another spike will occur about 3 weeks  when the Winter Olympics start.
- The country that searches for the Olympics the most is New Zealand but their spikes for the winter Olympics are much less than the U.S. and worldwide
- If you like at China's data for the Olympics since 2004 until about 2015 the amount of internet searches for the Olympics was miniscule but it has greatly increased since with a substantial spike for the 2016 summer Olympics
## Covid-19
- This data is from The New York Times not Google Trends
- Recently due to the Omicron Variant there has been a mssive spike in cases but deaths are still going down 
- The number of deaths seems to be directly correlated to our ability to treat the disease 
## Conclusions 
- All data that we looked at followed a pattern relative to events in the real  world and when something in the real world follows a pattern, such as the olympics 4 year cycle, the data follows the same pattern.
