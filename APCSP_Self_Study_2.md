# APCSP Practice Test Review
### Caleb O'Neal
**Question 1:** Binary Search<br>
**Question:** Suppose a list of numbers called dataset contains 2000 values. If the binary search algorithm was used to search the list for a value, what would be the maximum number of iterations necessary to successfully find this known value?<br>
**Answer:** 11<br>
**Explnation of answer:** A [Binary Search](https://en.wikipedia.org/wiki/Binary_search_algorithm) is a more effecient manner of identifying the loacation of a value in a list that works by halfing the possible locations of the value each iteration. A Binary Search requires that all values in the list be integers or floats and that they be in order from least to greatest, or greatest to least but it would require a flipped version of the algorithm. The easiest way to find the maximum number of iterations it would take to find an item with a Binary Search is to find the power of two that is greater than the number of elements in the array and the exxponent of that power of two will be your answer.<br>
**Explanation of my mistake:** I had no understanding os what a Binary Search is, a gap in my knowledge that has now been fixed<br>
**Question 2:** List Operations<br>
**Question:** Suppose a list called coefficients currently contains the following values: {1, 3, 5, 2, 4}.What is displayed after the following code segment is executed?

REMOVE(coefficients, 2)
APPEND(coefficients, 5)
INSERT(coefficients, 3, 11)
DISPLAY(coefficients[1] + coefficients[2] + LENGTH(coefficients))A

**Answer:** 12<br>
**Explanation of answer:** The remove command will remove the 3 from the list, the append then adds 5 to the end of the list, and finally the insert command will put an 11 at index 3 and our final list will be 1,5,11,2,4,5 our value at the inde of 1 is 1 and at two it is 5 and our length is 6 which when added together make 12.<br>
**Explanation of my mistake:** I misintepreted the remove statement which messed up my answer.<br>
**Question 3** Most Benefited from Distributed Computing<br>
**Question** Which of the following would be most benefited by using distributed computing?<br>
Select TWO answers.<br>
A. Controlling the aircraft that depart from and arrive at an international airport.<br>
B. Computing the average of a list of 20 million numbers.<br>
C. Operating an online game with hundreds of virtual players participating.<br>
D. Determining how many words in a thick book contain the vowel "a".<br>
**Answer:** A and C<br>
**Explanation of answer:** Both of these are [Distributed Computing](https://en.wikipedia.org/wiki/Distributed_computing) because they both rely on multiple devices e.g. the players computers and the host server, and the planes, ground control, and probably numerous other devices.<br>
**Explanation of my mistake:** I understood what Distributed Computing was but I didn't select two answers I only selected one. I also took this opprotunity learn more about Distributed Computing because it is fundamental to our modern society this is even shown in the question with the idera of planes being reliant on it.
